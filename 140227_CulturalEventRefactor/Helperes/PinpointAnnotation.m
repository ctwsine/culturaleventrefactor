//
//  PinpointAnnotation.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/4/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "PinpointAnnotation.h"

@interface PinpointAnnotation()
{
    NSString* _innerTitle;
    NSString* _innerDetailtitle;
}
@end

@implementation PinpointAnnotation

- (NSString*) title
{
    return _innerTitle;
}

- (NSString *)subtitle
{
    return _innerDetailtitle;
}

- (instancetype) initWithCoordinate2D: (CLLocationCoordinate2D) coord AndTitle: (NSString*) title AndSubTitle: (NSString*) subTitle
{
    self = [super init];
    if (self) {
        self.coordinate = coord;
        _innerDetailtitle = subTitle;
        _innerTitle = title;
    
    }
    return self;
}

+ (instancetype) annotationWithCoordinate2D: (CLLocationCoordinate2D) coord AndTitle: (NSString*) title AndSubTitle: (NSString*) subTitle
{
    return [[self alloc] initWithCoordinate2D:coord AndTitle:title AndSubTitle:subTitle];
}

@end
