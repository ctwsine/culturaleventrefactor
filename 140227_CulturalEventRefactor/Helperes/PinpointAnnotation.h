//
//  PinpointAnnotation.h
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/4/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PinpointAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;

- (instancetype) initWithCoordinate2D: (CLLocationCoordinate2D) coord AndTitle: (NSString*) title AndSubTitle: (NSString*) subTitle;

+ (instancetype) annotationWithCoordinate2D: (CLLocationCoordinate2D) coord AndTitle: (NSString*) title AndSubTitle: (NSString*) subTitle;

@end
