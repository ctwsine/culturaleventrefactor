//
//  VenueMainViewController.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/2/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "VenueMainViewController.h"
#import "VenueModel.h"
#import "VenueMapViewController.h"

@interface VenueMainViewController () <VenueModelDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) VenueModel* venueModel;
@property (nonatomic,strong) NSArray* venueList;
@property (nonatomic,strong) UITableView* tableView;

@end

@implementation VenueMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.venueModel fetchVenueList];
}

#pragma mark- getter
-(VenueModel *)venueModel
{
    if (!_venueModel) {
        _venueModel = [[VenueModel alloc] init];
        _venueModel.delegate = self;
    }
    return _venueModel;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        CGRect rect = self.view.frame;
        rect.origin.y = 66;
        _tableView = [[UITableView alloc] init];
        _tableView.frame = rect;
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

#pragma mark- setters
-(void)setVenueList:(NSArray *)venueList
{
    _venueList = venueList;
    [self.tableView reloadData];
}

#pragma mark- VenueModelDelegate
-(void)didFetchVenueList:(NSArray *)venueList
{
    self.venueList = venueList;
}

-(void)failToFetchVenueList:(NSError *)error
{
    NSLog(@"%@",error);
}

#pragma mark- TableViewDatasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.venueList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"venueTableCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"venueTableCell"];
    }
    VenueModel* venue = (VenueModel *)self.venueList[indexPath.row];
    cell.textLabel.text = venue.name;
    cell.detailTextLabel.text = venue.address;
    return cell;
}

#pragma mark- TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueModel* venue = self.venueList[indexPath.row];
    //vmvc.venue = venue;
    [self performSegueWithIdentifier:@"toVenueMapSegue" sender:venue];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    VenueMapViewController* vmvc = (VenueMapViewController* ) [segue destinationViewController];
    VenueModel* venue = (VenueModel*) sender;
    vmvc.venue = venue;
}

@end
