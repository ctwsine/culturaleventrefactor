//
//  EventDetailViewController.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 2/27/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "EventDetailViewController.h"


@interface EventDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@end

@implementation EventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.eventTitleLabel.text = self.eventModel.title;
    self.locationLabel.text = self.eventModel.location;
    self.eventTimeLabel.text = [NSString stringWithFormat:@"%@ - %@",self.eventModel.startDate,self.eventModel.endDate];
    self.contentTextView.text = self.eventModel.content;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Setter

-(void)setEventModel:(EventModel *)eventModel
{
    _eventModel = eventModel;
}

@end
