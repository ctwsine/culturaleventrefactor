//
//  VenueMapViewController.h
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/3/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenueModel.h"

@interface VenueMapViewController : UIViewController

@property (nonatomic,strong) VenueModel* venue;

@end
