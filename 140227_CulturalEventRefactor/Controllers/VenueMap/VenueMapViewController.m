//
//  VenueMapViewController.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/3/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "VenueMapViewController.h"
#import <MapKit/MapKit.h>
#import "PinpointAnnotation.h"

@interface VenueMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *venueMapView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextView *introTextView;

@end

@implementation VenueMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = self.venue.name;
    self.addressLabel.text = [NSString stringWithFormat:@"%@",self.venue.address];
    self.introTextView.text = self.venue.intro;
    [self setMapRegionWithLatitude:self.venue.latitude AndLongtitude:self.venue.longitude AndRadius:500 WithTitle:self.venue.name AndSubTitle:self.venue.address];
}

- (void)setMapRegionWithLatitude:(CGFloat) latitude AndLongtitude:(CGFloat) longtitude AndRadius: (int) radius WithTitle: (NSString*) title AndSubTitle: (NSString*) subTitle
{
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latitude, longtitude);
    CLLocationDistance width = radius;
    CLLocationDistance height = radius;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center, width, height);
    
    PinpointAnnotation* anno = [PinpointAnnotation annotationWithCoordinate2D:center AndTitle:title AndSubTitle:subTitle];
    
    [self.venueMapView addAnnotation:anno];
    [self.venueMapView setRegion:region];
}

@end
