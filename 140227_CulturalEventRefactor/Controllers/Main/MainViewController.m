//
//  MainViewController.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 2/27/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "MainViewController.h"
#import "EventModel.h"
#import "EventDetailViewController.h"
#import "NSString+Count.h"
@interface MainViewController () <EventModelDelegate,UITableViewDataSource,UITableViewDelegate>
{
    //EventModel* _eventModel;
//    NSArray* datasource;
//    UIActionSheet* actionSheet;
//    UIPopoverController* popovercontroller;
}

@property (nonatomic,strong) NSArray* eventList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) EventModel* eventModel;

@end

@implementation MainViewController


#pragma mark- Predefined menthods

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSLog(@"viewDidLoad called");
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.eventModel fetchEventList];
}

-(EventModel *)eventModel
{
    if (!_eventModel) {
        _eventModel = [[EventModel alloc]init];
        _eventModel.delegate = self;
    }
    return _eventModel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Event Model Delegate

- (void)didFetchEvent:(NSMutableArray *)eventList
{
    self.eventList = eventList;
}

- (void)failToFetchEvent:(NSError *)error
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"讀取資料遇到問題" message:@"喔喔...目前無法讀取來自文化部的資料，建議您檢查網路連線是否正常。" delegate:self cancelButtonTitle:@"知道啦" otherButtonTitles:nil ];
    [alert show];
}

#pragma mark- Setter
- (void)setEventList:(NSArray *)eventList
{
    _eventList = eventList;
    //NSLog(@"%@",_eventList);
    [self.tableView reloadData];
}

#pragma mark- TableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.eventList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"aloha"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"aloha"];
    }
    EventModel* event = self.eventList[indexPath.row];
    cell.textLabel.text = event.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",event.startDate,event.endDate];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventModel* event = (EventModel*) self.eventList[indexPath.row];
    [self performSegueWithIdentifier:@"goToEventDetail" sender:event];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //EventModel* event = sender;
    EventDetailViewController* edvc = (EventDetailViewController* )[segue destinationViewController];
    edvc.eventModel = (EventModel*) sender;
}

@end
