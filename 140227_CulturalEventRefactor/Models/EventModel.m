//
//  EventModel.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 2/27/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "EventModel.h"
#import <AFNetworking.h>
@implementation EventModel

- (instancetype) initWithJSONData: (id)data
{
    self = [super init];
    
    if (self) {
        self.title = data[@"title"];
        self.startDate = data[@"startDate"];
        self.endDate = data[@"endDate"];
        self.location = data[@"showInfo"][0][@"locationName"];
        self.content = data[@"descriptionFilterHtml"];
        self.category = [data[@"category"] integerValue];
    }
    return self;
}

+ (instancetype) eventWithJSONData: (id) data
{
    return [[EventModel alloc] initWithJSONData:data];
}

- (void)fetchEventList
{
    NSString* api = @"http://cloud.culture.tw/frontsite/trans/SearchShowAction.do?method=doFindTypeJ&keyword=2014";
    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:api parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableArray* eventList = @[].mutableCopy;
        [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            EventModel* event = [EventModel eventWithJSONData:obj];
            [eventList addObject:event];
        }];

        if ([self.delegate respondsToSelector:@selector(didFetchEvent:)]) {
            [self.delegate didFetchEvent:eventList];
        } else {
            NSLog(@"didFetchEvent: not implemented");
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(failToFetchEvent:)]) {
            [self.delegate failToFetchEvent:error];
        } else {
            NSLog(@"failToFetchEvent: not implemented");
        }
    }];
}

-(void) fetchEventListWithCategory: (NSInteger) category
{
    NSString* api = [NSString stringWithFormat:@"http://cloud.culture.tw/frontsite/trans/SearchShowAction.do?method=doFindTypeJ&category=%ld&keyword=2014",(long)category];
    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
    //
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //
    [manager GET:api parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"%@",responseObject);
        
        NSMutableArray* eventList = @[].mutableCopy;
        [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            EventModel* event = [EventModel eventWithJSONData:obj];
            [eventList addObject:event];
        }];
        
        if ([self.delegate respondsToSelector:@selector(didFetchEvent:)]) {
            [self.delegate didFetchEvent:eventList];
        } else {
            NSLog(@"didFetchEvent: not implemented");
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed: %@",error);
        if ([self.delegate respondsToSelector:@selector(failToFetchEvent:)]) {
            [self.delegate failToFetchEvent:error];
        } else {
            NSLog(@"failToFetchEvent: not implemented");
        }
    }];
    
}

@end
