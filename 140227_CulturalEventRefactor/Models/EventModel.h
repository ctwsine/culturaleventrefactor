//
//  EventModel.h
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 2/27/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EventModelDelegate <NSObject>

@optional
- (void) didFetchEvent:(NSMutableArray*)eventList;
- (void) failToFetchEvent:(NSError*)error;

@end


@interface EventModel: NSObject

@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSString* startDate;
@property (nonatomic,strong) NSString* endDate;
@property (nonatomic,strong) NSString* location;
@property (nonatomic,strong) NSString* content;
@property (nonatomic) NSInteger category;

@property (nonatomic,weak) id <EventModelDelegate> delegate;

-(void) fetchEventList;
-(void) fetchEventListWithCategory: (NSInteger) category;

@end
