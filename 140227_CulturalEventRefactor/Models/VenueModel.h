//
//  VenueModel.h
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/2/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VenueModelDelegate <NSObject>

- (void) didFetchVenueList: (NSMutableArray*) venueList;
- (void) failToFetchVenueList: (NSError*) error;

@end


@interface VenueModel : NSObject

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* address;
@property (nonatomic,strong) NSString* intro;
@property (nonatomic) CGFloat latitude;
@property (nonatomic) CGFloat longitude;

@property (nonatomic,weak) id <VenueModelDelegate> delegate;

- (void) fetchVenueList;
- (instancetype) initWithJSONData: (id) data;
+ (instancetype) venueWithJSONData: (id) data;

@end
