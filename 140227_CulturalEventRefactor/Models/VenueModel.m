//
//  VenueModel.m
//  140227_CulturalEventRefactor
//
//  Created by ctwsine on 3/2/14.
//  Copyright (c) 2014 ctwsine. All rights reserved.
//

#import "VenueModel.h"
#import <AFNetworking.h>
@implementation VenueModel

-(instancetype)initWithJSONData:(id)data
{
    self = [super init];
    if (self) {
        self.name = data[@"name"];
        self.address = data[@"address"];
        self.intro= data[@"intro"];
        self.latitude = [data[@"latitude"] floatValue];
        self.longitude = [data[@"longitude"] floatValue];
    }
    return self;
}

+ (instancetype)venueWithJSONData:(id)data
{
    return [[VenueModel alloc] initWithJSONData: data];
}

-(void)fetchVenueList
{
    NSString* api = @"http://cloud.culture.tw/frontsite/trans/emapOpenDataAction.do?method=exportEmapJsonByMainType&mainType=10";
    AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:api parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray* venueList = @[].mutableCopy;
        [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            VenueModel* venue = [VenueModel venueWithJSONData:obj];
            [venueList addObject:venue];
        }];
        
        if ([self.delegate respondsToSelector:@selector(didFetchVenueList:)]) {
            [self.delegate didFetchVenueList:venueList];
        } else {
            NSLog(@"didFetchVenueList not implemented");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(failToFetchVenueList:)]) {
            [self.delegate failToFetchVenueList:error];
        } else {
            NSLog(@"failToFetchVenueList not implemented");
        }
        
        NSLog(@"Failed");
    }];
}

@end
